import os
import re
import requests
import json
import pickle
import random
import sqlite3

import numpy as np
import pandas as pd
import matplotlib
import matplotlib.pyplot as plt
from flask import Flask, request, send_from_directory

from generate_neural_features import *
from kg_feature import kginfo
from utils import flatten_df

with open('model.pkl', 'rb') as file:
    PRED_MODEL = pickle.load(file)
with open('scaler.pkl', 'rb') as file:
    SCALER = pickle.load(file)

app = Flask(__name__)

def get_neural_features(premise, conclusion):
    p2c = zsl_multi_classifier(premise, conclusion, True)
    c2p = zsl_multi_classifier(premise, conclusion, True)
    return p2c, c2p

def make_pred(model, features):
    scaling_features = ['irrelevancy', 'aveDis']
    features[scaling_features] = SCALER.transform(features[scaling_features])
    input_array = flatten_df(features)
    actv = [input_array] + PRED_MODEL.coefs_
    actv = PRED_MODEL._forward_pass(actv)
    actv_frmtd = [list(l[0]) for l in actv]
    return actv_frmtd[-1][0], actv_frmtd[-1][1], actv_frmtd

def entity_info(entity_id):
    with sqlite3.connect('cache.db') as cache:
        cache.row_factory = sqlite3.Row
        c = cache.cursor()
        query = 'SELECT * FROM labels WHERE id == ?'
        c.execute(query, (entity_id,))
        pred = c.fetchone()
        if not pred is None:
            return pred['label'], pred['description']
        else:
            url = 'https://www.wikidata.org/wiki/Special:EntityData/' + entity_id + '.json'
            try:
                jsonInfo = requests.post(url).json()
                try:
                    label = jsonInfo['entities'][entity_id]['labels']['en']['value']
                except KeyError:
                    label = "?"
                try:
                    description = jsonInfo['entities'][entity_id]['descriptions']['en']['value']
                except KeyError:
                    description = '?'
            except Exception:
                label = "?"
                description = "?"
            new_values = (entity_id, label, description)
            sql = ''' INSERT INTO labels (id, label, description)
                      VALUES(?,?,?) '''
            c.execute(sql, new_values)
            cache.commit()
        return label, description

def get_node_info(eid, group):
    color_map = {'Premise': "#FFFBA1", 'Conclusion': "#A1EEFF", 'Intermediate': "#E5E5E5", 'Premise and conclusion': "#AFFFA1"}
    label, description = entity_info(eid)
    return {'id': eid, 'label': label, 'color': color_map[group], 'title': description, 'shape': 'box'}

def get_edge_info(s_id, eid, t_id):
    label, description = entity_info(eid)
    return {'id': s_id + '_' + eid + '_' + t_id, 'from': s_id, 'to': t_id, 'label': label, 'title': description}

def generate_network_graph(entities_p, entities_c, paths):
    nodes_P = [get_node_info(e, 'Premise') for e in entities_p]
    nodes_C = [get_node_info(e, 'Conclusion') for e in entities_c if not e in entities_p]
    nodes_PC = [get_node_info(e, 'Premise and conclusion') for e in entities_c if e in entities_p]
    parses = [l.split() for p in paths for l in p['newPaths'][0] if len(l.split()) == 3]
    nodes_I1 = [get_node_info(l[0], 'Intermediate') for l in parses]
    nodes_I2 = [get_node_info(l[2], 'Intermediate') for l in parses]

    all_nodes = list()
    ids_list = list()

    for n in nodes_PC:
        if not n['id'] in ids_list:
            all_nodes.append(n)
            ids_list.append(n['id'])
    for n in nodes_P:
        if not n['id'] in ids_list:
            all_nodes.append(n)
            ids_list.append(n['id'])
    for n in nodes_C:
        if not n['id'] in ids_list:
            all_nodes.append(n)
            ids_list.append(n['id'])
    for n in nodes_I1:
        if not n['id'] in ids_list:
            all_nodes.append(n)
            ids_list.append(n['id'])
    for n in nodes_I2:
        if not n['id'] in ids_list:
            all_nodes.append(n)
            ids_list.append(n['id'])

    edges = [get_edge_info(*p) for p in parses]
    edges = list([json.loads(e_2) for e_2 in set([json.dumps(e) for e in edges])])

    return all_nodes, edges

def get_node_colors(activ):
    flat_activ = [e for l in activ for e in l]
    flat_activ_norm = [e ** 2 / (1 + e ** 2) for e in flat_activ]
    Blues = plt.get_cmap('Blues')
    colors = [matplotlib.colors.rgb2hex(Blues(e)) for e in flat_activ_norm]
    return colors

@app.route('/')
def index():
    return app.send_static_file('index.html')

@app.route("/api/get_random", methods = ['POST'])
def get_random():
    with sqlite3.connect('cache.db') as cache:
        cache.row_factory = sqlite3.Row
        c = cache.cursor()
        c.execute('''SELECT * FROM predictions ORDER BY RANDOM() LIMIT 1;''')
        eg_pair = c.fetchone()
        result = {
            'premise': eg_pair['Premise'],
            'conclusion': eg_pair['Conclusion']
        }
    return result

@app.route("/api/get_prediction", methods = ['POST'])
def get_prediction():
    data = request.get_json()
    premise = data['premise']
    conclusion = data['conclusion']
    with sqlite3.connect('cache.db') as cache:
        cache.row_factory = sqlite3.Row
        c = cache.cursor()
        query = 'SELECT * FROM predictions WHERE Premise == ? AND Conclusion == ?'
        c.execute(query, (premise, conclusion))
        pred = c.fetchone()
        if not pred is None:
            print('Result found in cache')
            p2c = [round(float(t.replace('[', '').replace(']', '')), 3) for t in pred['EntailmentPtoC_distrib'].split() if not t in '[]']
            c2p = [round(float(t.replace('[', '').replace(']', '')), 3) for t in pred['EntailmentCtoP_distrib'].split() if not t in '[]']
            activ = eval(pred['activation'])
            result = {}
            result['val_pred'] = str(round(activ[-1][0], 3)),
            result['nov_pred'] = str(round(activ[-1][1], 3)),
            result['P2C_entail'] = str(p2c),
            result['C2P_entail'] = str(c2p),
            result['avg_dist'] = str(round(pred['avg_dist'], 3)),
            result['irrelevancy'] = str(round(pred['irrelevancy'], 3)),
            result['activations'] = json.dumps([round(e, 3) for l in eval(pred['activation']) for e in l]),
            result['activation_colors'] = json.dumps(get_node_colors(activ)),
            result['KG_nodes'] = json.dumps(eval(pred['nodes'])),
            result['KG_edges'] = json.dumps(eval(pred['edges']))
            return result
        else:
            print('Result not found in cache')
            irr, avg, paths, pids, cids = kginfo(premise, conclusion)
            p2c, c2p = get_neural_features(premise, conclusion)
            features = {
                'EntailmentPtoC_distrib' : [p2c],
                'EntailmentCtoP_distrib': [c2p],
                'irrelevancy': irr,
                'aveDis': avg
                }
            features = pd.DataFrame(features)
            val_pred, nov_pred, activ = make_pred(PRED_MODEL, features)
            nodes, edges = generate_network_graph(pids, cids, paths)
            p2c_rounded = [round(e, 3) for e in p2c.tolist()]
            c2p_rounded = [round(e, 3) for e in c2p.tolist()]
            results = {}
            results['val_pred'] = str(round(val_pred, 3))
            results['nov_pred'] = str(round(nov_pred, 3))
            results['P2C_entail'] = str(json.dumps(p2c_rounded))
            results['C2P_entail'] = str(json.dumps(c2p_rounded))
            results['avg_dist'] = str(round(avg, 3))
            results['irrelevancy'] = str(round(irr, 3))
            results['activations'] = json.dumps([round(e, 3) for l in activ for e in l]),
            results['activation_colors'] = json.dumps(get_node_colors(activ)),
            results['KG_nodes'] = json.dumps(nodes),
            results['KG_edges'] = json.dumps(edges)

            new_values = (premise, conclusion, str(p2c), str(c2p), avg, irr, str(activ), str(paths), str(nodes), str(edges))
            sql = ''' INSERT INTO predictions (Premise, Conclusion, EntailmentPtoC_distrib, EntailmentCtoP_distrib, avg_dist, irrelevancy, activation, paths, nodes, edges)
                      VALUES(?,?,?,?,?,?,?,?,?,?) '''
            c.execute(sql, new_values)
            cache.commit()

        return results