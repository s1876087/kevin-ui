from SPARQLWrapper import SPARQLWrapper, JSON
import re
import os
import requests
import urllib.parse
import json
import time
import sys
import pandas as pd


# set the global variable for the search depth limit, which is the maximum length of paths between entities.
DepthLimit = 3
PATH = '.'

def get_entities(query):
    if query == 'nan':
        return []
    query = urllib.parse.quote(query)
    r = requests.get(
        f"http://www.wikifier.org/annotate-article?text={query}&lang=en&userKey=gtgxqlsbixdzaqrofyjloembpkreik")
    dump = r.json()
    ids = []
    for item in dump['annotations']:
        if 'wikiDataItemId' in item.keys():
            ids.append((item['wikiDataItemId'], item['title']))

    return ids


# send SPARQL query to wikiData and return the answer
def ansWiki(query):
    try:
        # set an agent to avoid 403 HTTP ERROR
        sparql = SPARQLWrapper("https://query.wikidata.org/sparql",
                               agent='Mozilla/5.0 (Macintosh; Intel Mac OS X 10_11_5) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/50.0.2661.102 Safari/537.36')
        sparql.setReturnFormat(JSON)
        sparql.setQuery(query)
        sparql.setTimeout(60)  # 5 minutes
        # Get the answering {'head': {'vars': [...]}, 'results': {'bindings': [...]}}
        ans = sparql.query().convert()
    except:
        ans = {'results': {'bindings': []}}

    return ans

# find all paths between a member of fromIds to a member of toIds, where the paths length is smaller than the DepthLimit
# return a list of dictionaries representing shortest paths
def get_path(fromIds, toIds, logRList, diffRList):
    i = 0
    output = []

    duplicates = list(set(fromIds).intersection(toIds))
    # one-node path for one entity to itself with the minimal path length as 0
    for x in duplicates:
        mapInfo = {'from': x, 'to': x, 'paths': [[x]]}
        output.append(mapInfo)

    # otherwise, find closest path with sparql query
    restToIds = [id for id in toIds if id not in duplicates]
    while i < DepthLimit and restToIds != []:
        queryHead = "SELECT ?from ?to"
        fromValues = ' wd:'.join(fromIds)
        toValues = ' wd:'.join(restToIds)
        queryValues = f"\n VALUES ?from {{ wd:{fromValues} }} \n VALUES ?to {{ wd:{toValues} }} "
        queryMain = '\n' + 'WHERE\n{' + queryValues + '\n ?from'
        for j in range(i):
            # pair the varialbes of the relation and the objection
            pair = '?r' + str(j) + ' ' + '?id' + str(j)
            queryHead += pair
            # the current objection will be the subjection of the next triple, placed after '\n'
            queryMain += ' ' + pair + '.\n  ' + ' ?id' + str(j)

        queryHead += ' ?r' + str(i)
        queryMain += ' ?r' + str(i) + '?to.'
        queryEnd = '\n}'
        query = queryHead + queryMain + queryEnd
        result = ansWiki(query.encode('utf-8'))['results']

        checkedToIds = []
        # Check the answer
        if result['bindings'] == []:
            i += 1  # continue to the next step
        else:
            # store found paths in a list.
            for rawList in result['bindings']:  # rawList = {{id0...}, {id1...},...{idj...}, {r0...},...{rj+1}}
                paths = []  # initialise the output lists as empty
                startId = rawList['from']['value'].split('/')[-1]
                toId = rawList['to']['value'].split('/')[-1]
                checkedToIds.append(toId)
                path = startId + ' '
                # Construct the path by appending the jth triple. Each triple are separated by a comma.
                for j in range(i):
                    # pair the varialbes of the relation and the objection
                    relation = rawList['r' + str(j)]['value'].split('/')[-1]
                    objId = rawList['id' + str(j)]['value'].split('/')[-1]
                    # the object of this triple will be the subject of the next triple
                    path += (relation + ' ' + objId + ', ' + objId + ' ')
                path += (rawList['r' + str(i)]['value'].split('/')[-1] + ' ' + str(toId))
                paths.append(path.split(', '))
                #minsemPathLen, semPath = semanticPath(paths, logRList, diffRList)
                mapInfro = {'from': startId, 'to': toId, 'paths': paths}
                if mapInfro not in output:
                    output.append(mapInfro)

            i += 1
            restToIds = [id for id in restToIds if id not in checkedToIds]

    return output


# calculate the graph distance between the given conclusion and the premise
# the entities in conclusion that are irrelevant from the premise need to be counted separately
def kgPath(premise, conclusion, logRList, diffRList):
    minDis = maxDis = aveDis = DepthLimit + 1
    sumDis = 0
    irrelevancy = 0  # record the number of entities in the conclusion that have no path with entities in premise
    pIds = [j for (j, _) in get_entities(premise)]  # get wikiDataIds from premises
    cIds = [j for (j, _) in get_entities(conclusion)]  # get wikiDataIds from conclusion
    pathsDic = get_path(pIds, cIds, logRList, diffRList)
    if pathsDic == []:
         return pIds, cIds, len(cIds), pathsDic
    else:
        irrelevancy = len([id for id in cIds if id not in [p['to'] for p in pathsDic]])
        return pIds, cIds, irrelevancy, pathsDic

# calculate the semantic paths and their length
def semanticPath(pathsDicList, logRList, diffRList, sims):
    pathNewList = []

    for pathDic in pathsDicList:
        newPathDic = pathDic.copy()
        pathList = pathDic['paths']
        newPathList = []

        singleNodePath = 0
        for path in pathList:
            newPath = []
            invalid = 0
            for triple in path:
                # ignore logical relation by remove the step
                if ' ' not in triple:
                    singleNodePath = 1
                    newPath.append(triple)
                elif triple.split()[1] in diffRList:
                    invalid = 1
                    #print('\n   invalid  \n' + str(triple))
                elif triple.split()[1] not in logRList:
                    newPath.append(triple)
            # no different from
            if invalid != 1:
                # have found the shortest path: [].
                if newPath == []:
                    newPathList = [[]]
                    break
                elif newPathList == [] or len(newPathList[0]) > len(newPath):
                    newPathList = [newPath]

        newPathDic['newPaths'] = sorted(newPathList)
        if newPathList != []:
            if singleNodePath == 1:
                newPathDic['minpathLen'] = 0
            # adjustment
            elif [] in  newPathDic['newPaths']:
                newPathDic['minpathLen'] = sims
            else:
                newPathDic['minpathLen'] = len(newPathDic['newPaths'][0])
        else:
            newPathDic['minpathLen'] = 4
        if newPathDic not in pathNewList:
            pathNewList.append(newPathDic)

    return pathNewList

# calculte the average distance
def aveDist(pIds, cIds, paths):
    paths = pd.DataFrame(paths)
    total = 0
    for p in pIds:
        for c in cIds:
            if len(paths) != 0:
                sub_paths = paths[(paths['from'] == p) & (paths['to'] == c)]
            if len(paths) == 0 or len(sub_paths) == 0:
                total += DepthLimit+1
            else:
                total += sub_paths['minpathLen'].min()
    return total / (len(cIds) * len(pIds))

def kginfo(premise, conclusion):
    # get lists of extreme relations
    logRList = ['P31', 'P279', 'P527', 'P361', 'P463', 'P1269', 'P355', 'P460', 'P642']
    diffRList = ['P1889', 'P461']
    pIds, cIds, irrelevancy, pathsDic = kgPath(premise, conclusion, logRList, diffRList)
    semPaths= semanticPath(pathsDic, logRList, diffRList, 0)
    aveDistance = aveDist(pIds, cIds, semPaths)

    return irrelevancy, aveDistance, semPaths, pIds, cIds
