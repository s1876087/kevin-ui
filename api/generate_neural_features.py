from transformers import AutoModelForSequenceClassification, AutoTokenizer
import torch

DEVICE = "cuda:0" if torch.cuda.is_available() else "cpu"
BART_CLS = AutoModelForSequenceClassification.from_pretrained('facebook/bart-large-mnli').to(DEVICE)
BART_TKN = AutoTokenizer.from_pretrained('facebook/bart-large-mnli')

def zsl_multi_classifier(sequence, context, distrib):
    x = BART_TKN.encode(sequence, context, return_tensors='pt',
                         truncation_strategy='only_first').to(DEVICE)
    if distrib:
        results = torch.nn.functional.softmax(BART_CLS(x)[0], -1).squeeze(0).cpu().detach().numpy()
    else:
        results = BART_CLS(x)[0].argmax(-1).item()
    return results
