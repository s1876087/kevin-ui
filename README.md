# kevin-ui


To set up the project after cloning the repository: 

1) Install Javascript dependencies:
```
cd kevin-ui
yarn install
```

2) Create Python virtual environment and install dependencies:
```
cd api
python3 -m venv venv
source venv/bin/activate
pip install -r requirements.txt
```

3) Launch React app:
```
cd ..       # you should be at the root of the kevin-ui repo once again
yarn start
```

4) Launch Flask back-end in a new terminal:
```
cd kevin-ui
yarn start-api
```

5) Access the app from http://localhost:3000/ in your browser
