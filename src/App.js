import React, { useState, useEffect } from 'react';
import {Input, Button, Segment, Table, Form, TextArea} from 'semantic-ui-react';
import Plot from 'react-plotly.js'
import 'semantic-ui-css/semantic.min.css'
import './App.css';
import Network from "vis-react";

function App() {
    const [displayMessage, setDisplayMessage] = useState("Write your own premise/conclusion pair in the input boxes above, or click on \"Get random pair\". Then, submit to see my prediction with an explanation!")
    const [premise, setPremise] = useState("");
    const [conclusion, setConclusion] = useState("");
    const [input, setInput] = useState({
        "premise": "",
        "conclusion": "",
    });
    const [val, setVal] = useState(null);
    const [nov, setNov] = useState(null);
    const [neuralFeatures, setNeuralFeatures] = useState({
        "P2C_cont": null,
        "P2C_neut": null,
        "P2C_ent": null,
        "C2P_cont": null,
        "C2P_neut": null,
        "C2P_ent": null,
    });
    const [KGFeatures, setKGFeatures] = useState({
        "Avg_dist": null,
        "Irrelevancy": null,
    });
    const [randomLoading, setRandomLoading] = useState(false)
    const [loading, setLoading] = useState(false)
    const [explanationVisibility, setExplanationVisibility] = useState({
        "neural": false,
        "kg": false,
        "nn": false
    });
    const [activations, setActivations] = useState([]);
    const [nodeColors, setNodeColors] = useState([]);

    const [windowSize, setWindowSize] = useState([
        window.innerWidth,
        window.innerHeight,
    ]);

    const [graphNodes, setGraphNodes] = useState(null);
    const [graphEdges, setGraphEdges] = useState(null);

    useEffect(() => {
        const handleWindowResize = () => {
            setWindowSize([window.innerWidth, window.innerHeight]);
        };

        window.addEventListener('resize', handleWindowResize);

        return () => {
            window.removeEventListener('resize', handleWindowResize);
        };
    });

    const getRandom = async () => {
        const response = await fetch('/api/get_random', {
            method: 'POST',
            headers: {
                'Content-Type': 'application/json'
            },
        });

        if (response.ok) {
            console.log('Random article response ok')
            response.json().then((data => {
                setPremise(data.premise)
                setConclusion(data.conclusion)
            }))
            setRandomLoading(false)
            setDisplayMessage("Write your own premise/conclusion pair in the input boxes above, or click on \"Get random pair\". Then, submit to see my prediction with an explanation!")
        } else {
            console.log("responses didnt work")
            setRandomLoading(false)
            setDisplayMessage("Query did not work...")
            setVal(null)
            setNov(null)
        }
    }

    const getPrediction = async () => {
        const response = await fetch('/api/get_prediction', {
            method: 'POST',
            headers: {
                'Content-Type': 'application/json'
            },
            body: JSON.stringify({"premise": premise, "conclusion": conclusion})
        })

        if (response.ok) {
            response.json().then((data) => {
                setVal(data.val_pred)
                setNov(data.nov_pred)
                setNeuralFeatures(
                    {
                        "P2C_cont": JSON.parse(data.P2C_entail)[0],
                        "P2C_neut": JSON.parse(data.P2C_entail)[1],
                        "P2C_ent": JSON.parse(data.P2C_entail)[2],
                        "C2P_cont": JSON.parse(data.C2P_entail)[0],
                        "C2P_neut": JSON.parse(data.C2P_entail)[1],
                        "C2P_ent": JSON.parse(data.C2P_entail)[2],
                    }
                )
                setKGFeatures(
                    {
                        "Avg_dist": JSON.parse(data.avg_dist),
                        "Irrelevancy": JSON.parse(data.irrelevancy),
                    }
                )
                setActivations(JSON.parse(data.activations))
                setNodeColors(JSON.parse(data.activation_colors))
                setGraphNodes(JSON.parse(data.KG_nodes))
                setGraphEdges(JSON.parse(data.KG_edges))
                setExplanationVisibility({
                    "neural": true,
                    "kg": false,
                    "nn": false
                })
                setLoading(false)
            })
            setInput({"premise": premise, "conclusion": conclusion})
            setPremise('')
            setConclusion('')
            console.log("responses worked")
            setDisplayMessage("Write your own premise/conclusion pair in the input boxes above, or click on \"Get random pair\". Then, submit to see my prediction with an explanation!")
        } else {
            console.log("responses didnt work")
            setLoading(false)
            setDisplayMessage("Query did not work...")
            setVal(null)
            setNov(null)
        }
    }

    const renderPrediction = () => {
        // var parse = require('html-react-parser')
        if (val != null && nov != null && !loading) {
            if (val >= 0.5) {
                var val_text = "VALID"
                var val_col = "green"
            } else if (val < 0.5) {
                var val_text = "NOT VALID"
                var val_col = "red"
            }

            if (nov >= 0.5) {
                var nov_text = "NOVEL"
                var nov_col = "green"
            } else if (nov < 0.5) {
                var nov_text = "NOT NOVEL"
                var nov_col = "red"
            }

            return (
                <div className = "PageBody">
                    <div className="ArgumentDisplay">
                        <p className="Argument">
                            <b>Premise</b>: {input.premise}<br/><br/>
                            <b>Conclusion</b>: {input.conclusion}
                        </p>
                    </div>
                    <div className="Prediction">
                        <p>I believe this argument is <Button active={true} color={val_col}><b>{val_text}</b></Button> and <Button active={true} color={nov_col}><b>{nov_text}</b></Button></p>
                    </div>
                    <div className="Explanation">
                        <Button.Group attached='top'>
                            <Button
                                onClick={() => setExplanationVisibility(
                                    {
                                        "neural": true,
                                        "kg": false,
                                        "nn": false
                                    }
                                )}
                                active={explanationVisibility["neural"]}
                            >Neural features
                            </Button>
                            <Button
                                onClick={() => setExplanationVisibility(
                                    {
                                        "neural": false,
                                        "kg": true,
                                        "nn": false
                                    }
                                )}
                                active={explanationVisibility["kg"]}
                            >Knowledge graph features
                            </Button>
                            <Button
                                onClick={() => setExplanationVisibility(
                                    {
                                        "neural": false,
                                        "kg": false,
                                        "nn": true,
                                    }
                                )}
                                active={explanationVisibility["nn"]}
                            >Final classifier
                            </Button>
                        </Button.Group>
                        {renderNeural()}
                        {renderKg()}
                        {renderNN()}
                    </div>
                </div>
            )
        } else {
            return (
                <div className="WelcomeMessage">
                    <h2>{displayMessage}</h2>
                </div>
            )
        }
    }

    const renderNeural = () => {
        if (explanationVisibility['neural']) {
            return (
                <div>
                    <Segment attached className = "Explanation">
                        <p className="TextExplanation">
                            In order to determine whether the input argument (i.e. premise and argument) is valid,
                            we first represent this argument as a set of eight numerical features. Six of these features
                            are obtained using a large pre-trained language model called BART_MNLI. Because this
                            model is a neural network, we call these "neural features".<br/><br/>

                            This model is designed to detect whether a first piece of text (i.e. the premise) entails, contradicts, or is neutral with
                            respect to a second piece of text (i.e. the conclusion). Concretely, the model outputs three
                            positive real numbers adding up to 1, representing the probability of each class.
                        </p>
                        <div className="FeatureDisplay">
                            <Table celled>
                                <Table.Header>
                                    <Table.Row>
                                        <Table.HeaderCell textAlign={"center"}>Premise contradicts conclusion</Table.HeaderCell>
                                        <Table.HeaderCell textAlign={"center"}>Premise is neutral w.r.t. conclusion</Table.HeaderCell>
                                        <Table.HeaderCell textAlign={"center"}>Premise entails conclusion</Table.HeaderCell>
                                    </Table.Row>
                                </Table.Header>

                                <Table.Body>
                                    <Table.Row>
                                        <Table.Cell textAlign={"center"}>{neuralFeatures['P2C_cont']}</Table.Cell>
                                        <Table.Cell textAlign={"center"}>{neuralFeatures['P2C_neut']}</Table.Cell>
                                        <Table.Cell textAlign={"center"}>{neuralFeatures['P2C_ent']}</Table.Cell>
                                    </Table.Row>
                                </Table.Body>
                            </Table>
                        </div>

                        <p className="TextExplanation">
                            The BART_MNLI model returns different probabilities depending on the order of the
                            two extracts passed as input. By passing the conclusion as the first extract, and the
                            premise as the second, we therefore obtain three additional features:
                        </p>
                        <div className="FeatureDisplay">
                            <Table celled>
                                <Table.Header>
                                    <Table.Row>
                                        <Table.HeaderCell textAlign={"center"}>Conclusion contradicts premise</Table.HeaderCell>
                                        <Table.HeaderCell textAlign={"center"}>Conclusion is neutral w.r.t. premise</Table.HeaderCell>
                                        <Table.HeaderCell textAlign={"center"}>Conclusion entails premise</Table.HeaderCell>
                                    </Table.Row>
                                </Table.Header>

                                <Table.Body>
                                    <Table.Row>
                                        <Table.Cell textAlign={"center"}>{neuralFeatures['C2P_cont']}</Table.Cell>
                                        <Table.Cell textAlign={"center"}>{neuralFeatures['C2P_neut']}</Table.Cell>
                                        <Table.Cell textAlign={"center"}>{neuralFeatures['C2P_ent']}</Table.Cell>
                                    </Table.Row>
                                </Table.Body>
                            </Table>
                        </div>
                    </Segment>

                </div>
            )
        }
    }

    const renderKg = () => {
        if (explanationVisibility['kg']) {
            return (
                <Segment attached className = "Explanation">
                    <p className="TextExplanation">
                        A Knowledge Graph (KG) represents a network of real-world entities—i.e. objects, events,
                        situations, or concepts—and illustrates the relationship between them. This information can be
                        visualized as a graph structure.<br/><br/>
                        The following visualisation represents all the entities found in the input premise and
                        conclusion, as well as the paths that connect them, either directly or via intermediate
                        entities. The length of paths is limited 3 in this implementation.
                    </p>
                    <div className="NetworkGraph">
                        <Network
                            graph={{
                                nodes: graphNodes,
                                edges: graphEdges
                            }}
                            options = {{
                                autoResize: true,
                                interaction: {
                                    hover: true,
                                    tooltipDelay: 1
                                }
                            }}
                            style={{ height: 500}}
                        />
                    </div>
                    <div className="Legend">
                        <Button active={true} style={{backgroundColor: "#FFFBA1"}}><b>Premise</b></Button>
                        <Button active={true} style={{backgroundColor: "#A1EEFF"}}><b>Conclusion</b></Button>
                        <Button active={true} style={{backgroundColor: "#AFFFA1"}}><b>Premise & Conclusion</b></Button>
                        <Button active={true} style={{backgroundColor: "#E5E5E5"}}><b>Intermediate</b></Button>
                    </div>
                    <p className="TextExplanation">
                        Given this KG representation, we can calculate the following two features to help us
                        determine the validity and novelty of our input argument:
                        <ul>
                            <li><b>Irrelevancy</b>: the number of entities from the conclusion that have no KG path to
                                any entity in the premise;</li>
                            <li><b>Average distance</b>: the average length of the paths connecting all entities from
                                the premise and all entities from the conclusion (path that do not exist have a length of 4 by default).</li>
                        </ul>
                    </p>
                    <div className="FeatureDisplay">
                        <Table celled>
                            <Table.Header>
                                <Table.Row>
                                    <Table.HeaderCell textAlign={"center"}>Average distance</Table.HeaderCell>
                                    <Table.HeaderCell textAlign={"center"}>Irrelevancy</Table.HeaderCell>
                                </Table.Row>
                            </Table.Header>

                            <Table.Body>
                                <Table.Row>
                                    <Table.Cell textAlign={"center"}>{KGFeatures['Avg_dist']}</Table.Cell>
                                    <Table.Cell textAlign={"center"}>{KGFeatures['Irrelevancy']}</Table.Cell>
                                </Table.Row>
                            </Table.Body>
                        </Table>
                    </div>
                </Segment>
            )
        }
    }

    const renderNN = () => {
        var sources = [0, 0, 0, 0, 0, 1, 1, 1, 1, 1, 2, 2, 2, 2, 2, 3, 3, 3, 3, 3, 4, 4, 4, 4, 4, 5, 5, 5, 5, 5, 6, 6, 6, 6, 6, 7, 7, 7, 7, 7, 8, 8, 8, 8, 8, 9, 9, 9, 9, 9, 10, 10, 10, 10, 10, 11, 11, 11, 11, 11, 12, 12, 12, 12, 12, 13, 13, 14, 14, 15, 15, 16, 16, 17, 17]
        var targets = [8, 9, 10, 11, 12, 8, 9, 10, 11, 12, 8, 9, 10, 11, 12, 8, 9, 10, 11, 12, 8, 9, 10, 11, 12, 8, 9, 10, 11, 12, 8, 9, 10, 11, 12, 8, 9, 10, 11, 12, 13, 14, 15, 16, 17, 13, 14, 15, 16, 17, 13, 14, 15, 16, 17, 13, 14, 15, 16, 17, 13, 14, 15, 16, 17, 18, 19, 18, 19, 18, 19, 18, 19, 18, 19]
        var weights = [0.471, 0.431, 0.268, 0.056, 2.624, 0.507, 0.693, 0.072, 0.813, 0.563, 0.048, 0.47, 0.764, 0.552, 0.934, 0.706, 0.021, 0.485, 0.799, 0.035, 0.667, 0.189, 0.081, 0.121, 0.668, 1.208, 1.34, 0.521, 2.602, 0.799, 0.317, 0.613, 0.432, 0.26, 0.207, 0.717, 0.574, 0.814, 0.246, 0.3, 0.0, 0.0, 1.266, 1.046, 0.35, 0.0, 0.0, 0.412, 0.258, 0.435, 0.0, 0.0, 0.084, 1.071, 0.703, 0.0, 0.0, 0.993, 0.528, 0.078, 0.0, 0.0, 0.081, 0.896, 0.645, 0.0, 0.0, 0.0, 0.0, 0.548, 0.791, 1.958, 1.867, 0.659, 0.19]
        var weights_w_sign = [-0.471, 0.431, -0.268, 0.056, -2.624, -0.507, -0.693, -0.072, 0.813, 0.563, 0.048, 0.47, 0.764, -0.552, 0.934, 0.706, -0.021, -0.485, 0.799, -0.035, -0.667, 0.189, -0.081, 0.121, 0.668, 1.208, -1.34, -0.521, -2.602, -0.799, -0.317, -0.613, 0.432, 0.26, -0.207, 0.717, -0.574, -0.814, 0.246, 0.3, -0.0, 0.0, 1.266, -1.046, 0.35, -0.0, -0.0, -0.412, 0.258, 0.435, 0.0, 0.0, 0.084, -1.071, 0.703, 0.0, 0.0, 0.993, 0.528, 0.078, -0.0, -0.0, -0.081, -0.896, 0.645, 0.0, 0.0, -0.0, -0.0, -0.548, 0.791, -1.958, -1.867, 0.659, -0.19]
        var colors = ['pink', 'lightsteelblue', 'pink', 'lightsteelblue', 'pink', 'pink', 'pink', 'pink', 'lightsteelblue', 'lightsteelblue', 'lightsteelblue', 'lightsteelblue', 'lightsteelblue', 'pink', 'lightsteelblue', 'lightsteelblue', 'pink', 'pink', 'lightsteelblue', 'pink', 'pink', 'lightsteelblue', 'pink', 'lightsteelblue', 'lightsteelblue', 'lightsteelblue', 'pink', 'pink', 'pink', 'pink', 'pink', 'pink', 'lightsteelblue', 'lightsteelblue', 'pink', 'lightsteelblue', 'pink', 'pink', 'lightsteelblue', 'lightsteelblue', 'pink', 'lightsteelblue', 'lightsteelblue', 'pink', 'lightsteelblue', 'pink', 'pink', 'pink', 'lightsteelblue', 'lightsteelblue', 'lightsteelblue', 'lightsteelblue', 'lightsteelblue', 'pink', 'lightsteelblue', 'lightsteelblue', 'lightsteelblue', 'lightsteelblue', 'lightsteelblue', 'lightsteelblue', 'pink', 'pink', 'pink', 'pink', 'lightsteelblue', 'lightsteelblue', 'lightsteelblue', 'pink', 'pink', 'pink', 'lightsteelblue', 'pink', 'pink', 'lightsteelblue', 'pink']
        var nodes = ['P2C_contradiction', 'P2C_neutral', 'P2C_entailment', 'C2P_contradiction', 'C2P_neutral', 'C2P_entailment', 'Average distance', 'Irrelevancy', '', '', '', '', '', '', '', '', '', '', 'Validity', 'Novelty']
        if (explanationVisibility['nn']) {
            return (
                <div>
                    <Segment attached className = "Explanation">
                        <p className="TextExplanation">
                            Having generated our 8 features (6 using a large pre-trained neural network and 2 using a
                            Knowledge Graph) we pass these features to our final classifier, which we have previously
                            trained on a dedicated training dataset. This classifier is a simple Neural Network. It
                            takes our 8 features as input and transforms them mathematically to obtain two output
                            numbers: 1) the probability of the argument being valid and 2) the probability of the argument
                            being novel.
                        </p>
                        <div className="FeatureDisplay">
                            <Table celled>
                                <Table.Header>
                                    <Table.Row>
                                        <Table.HeaderCell textAlign={"center"}>Validity probability</Table.HeaderCell>
                                        <Table.HeaderCell textAlign={"center"}>Novelty probability</Table.HeaderCell>
                                    </Table.Row>
                                </Table.Header>

                                <Table.Body>
                                    <Table.Row>
                                        <Table.Cell textAlign={"center"}>{val}</Table.Cell>
                                        <Table.Cell textAlign={"center"}>{nov}</Table.Cell>
                                    </Table.Row>
                                </Table.Body>
                            </Table>
                        </div>
                        <p className="TextExplanation">
                            Some of the mathematical transformations the neural network performs are represented
                            visually in the following diagram. Each edge in the diagram represents a weight, which
                            multiplies the node on its left. This product then feeds into the right-hand node: i.e. each
                            node (excluding the leftmost nodes) is then the sum of all incoming products. Finally, this
                            sum must exceed a particular threshold to be passed to the next neuron: otherwise it is set
                            to 0. <br/><br/>
                            Red edges are negative weights, while blue edges are positive. Their width represents their
                            magnitude. Moreover, the colour of each node represents its value. Hover over each element
                            to learn more.<br/><br/>
                        </p>
                        <div className="SankeyDiagram">
                            <Plot
                                data={[
                                    {
                                        type: "sankey",
                                        orientation: "h",
                                        node: {
                                            pad: 15,
                                            thickness: 20,
                                            line: {
                                                color: "black",
                                                width: 0.5
                                            },
                                            label: nodes,
                                            color: nodeColors,
                                            customdata: activations,
                                            hovertemplate: 'This neuron outputs a value of %{customdata}',
                                        },

                                        link: {
                                            source: sources,
                                            target: targets,
                                            value:  weights,
                                            color: colors,
                                            customdata: weights_w_sign,
                                            hovertemplate: 'This edge has value %{customdata}'
                                        }
                                    }
                                ]}
                                layout={ { width: 0.8 * windowSize[0], height: 0.5 * windowSize[0] } }
                            />
                        </div>
                    </Segment>

                </div>
            )
        }
    }

    return (
        <div className="App">
            <header className="Input">
                <div className = "PageTitle">
                  <h1>KEViN: a Knowledge Enhanced Validity and Novelty classifier</h1>
                </div>
                <Form className="UserInput">
                    <TextArea
                        placeholder='Premise'
                        type="text"
                        value={premise}
                        onChange={e => setPremise(e.target.value)}
                        className = "PremiseInput"
                        style={{ minHeight: 150 }}
                    />
                </Form>
                <Input
                    placeholder = "Conclusion"
                    type="text"
                    value={conclusion}
                    onChange={e => setConclusion(e.target.value)}
                    className = "UserInput"
                />
                <div className = "HeaderButtons">
                    <Button
                        onClick = {() => {
                          setRandomLoading(true)
                          getRandom()
                        }}
                        disabled = {(loading)}
                        loading = {randomLoading}
                    >Get random pair</Button>
                    <Button
                        onClick = {() => {
                          setLoading(true)
                          getPrediction()
                        }}
                        disabled = {premise === '' || conclusion == ''}
                        color = 'blue'
                        loading = {(loading)}
                    >Submit</Button>
                </div>
            </header>
            {renderPrediction()}
        </div>
    );
}

export default App;